package org.wlhlearn.dubboDemo1;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.wlhlearn.dubboDemo.IPayService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );


        ClassPathXmlApplicationContext classPathXmlApplicationContext=
                new ClassPathXmlApplicationContext(new String[]{"META-INF/spring/application.xml"});
        IPayService payService=(IPayService)classPathXmlApplicationContext.getBean("payService");
         String rs=  payService.doPay("mic");
        System.out.println(rs);
        System.out.println(payService.doPay("jhfuewhwfywfef"));
    }
}
