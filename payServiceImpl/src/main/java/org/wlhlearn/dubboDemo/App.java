package org.wlhlearn.dubboDemo;

import org.apache.dubbo.container.Main;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        /*System.out.println( "Hello World!" );

        ClassPathXmlApplicationContext classPathXmlApplicationContext=
                new ClassPathXmlApplicationContext(new String[]{"META-INF/spring/application.xml"});
        classPathXmlApplicationContext.start();*/

        // 可以采用dubbo 提供的方式来启动  实际是一样的效果
        Main.main(args);

        System.in.read();
    }
}
