package org.wlhlearn.dubboDemo;

public interface IPayService {

    String doPay(String info);
}
